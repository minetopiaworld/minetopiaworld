package eu.minetopiaworld.plugin.util;

public class Placeholder {
	/** Scoreboard */
	static final String LOCATION = "<Locatie>";
	static final String WORLD_COLOR = "<WereldKleur>";
	static final String OPPOSITE_WORLD_COLOR = "<TegenWereldKleur>";
	static final String FITNESS = "<Fitheid>";
	static final String WORLDSHARDS = "<WorldShards>";
	static final String GOLDWORLDSHARDS = "<GoldWorldShards>";
	static final String DATE = "<Datum>";
	static final String TIME = "<Tijd>";
	static final String MAX_FITNESS = "<MaxFit>";

	/** Plots */
	static final String PLOT_PRICE = "<Prijs>";
	static final String PLOT_NAME = "<PlotNaam>";
	static final String OWNER = "<Eigenaar>"; // Might not be needed
	static final String MEMBERS = "<Leden>"; // Might not be needed
	static final String DOORPOINT = "<Stad>";
	

	/** Item */
	static final String CELLPHONE_NUMBER = "<Mobiel>"; // Might not be needed

	/** Misc */
	static final String EMPTY_LINE = "<LegeRegel>";

	
}