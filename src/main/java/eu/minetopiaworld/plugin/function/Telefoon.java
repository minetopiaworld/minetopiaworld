package eu.minetopiaworld.plugin.function;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Telefoon implements Listener {
	private static boolean bellenBool = false;

	@SuppressWarnings("unused")
	@EventHandler
	public void onJoinEvent(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		ItemStack telefoon = new ItemStack(Material.BLAZE_ROD, 1);
		PlayerInventory inv = e.getPlayer().getInventory();
		inv.setItem(8, telefoon);
	}

	public static boolean isBellen() {
		return bellenBool;
	}

	public static boolean setBellen(boolean bellen) {
		bellenBool = bellen;
		return bellen;
	}
}