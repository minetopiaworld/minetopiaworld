package eu.minetopiaworld.plugin.function;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class Prullenbak implements Listener {
	Inventory trashCan = Bukkit.createInventory(null, 18);
	private List<UUID> openedInvList = new ArrayList<>();

	@EventHandler
	public void onBlockClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			Material m = e.getClickedBlock().getType();
			if (m.equals(Material.DROPPER)) {
				p.openInventory(trashCan);
				for (Player player : Bukkit.getServer().getOnlinePlayers()) {
					if (openedInvList.contains(player.getUniqueId())) {
						player.closeInventory();
					}
				}
				this.openedInvList.clear();
			}
		}
	}
}