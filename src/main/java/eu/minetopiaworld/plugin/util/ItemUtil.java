package eu.minetopiaworld.plugin.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

/**
 * Easily create itemstacks, without messing your hands. <i>Note that if you do
 * use this in one of your projects, leave this notice.</i> <i>Please do credit
 * me if you do use this in one of your projects.</i>
 * 
 * @author NonameSL
 * @author SBDeveloper [1.13.x support]
 * @author FinnB05 [Unbreaking + Added 1.14+ support]
 */
public class ItemUtil {
	private ItemStack is;

	/**
	 * Create a new ItemBuilder from scratch.
	 * 
	 * @param m The material to create the ItemBuilder with.
	 */
	public ItemUtil(Material m) {
		this(m, 1);
	}

	/**
	 * Create a new ItemBuilder over an existing itemstack.
	 * 
	 * @param is The itemstack to create the ItemBuilder over.
	 */
	public ItemUtil(ItemStack is) {
		this.is = is;
	}

	/**
	 * Create a new ItemBuilder from scratch.
	 * 
	 * @param m      The material of the item.
	 * @param amount The amount of the item.
	 */
	public ItemUtil(Material m, int amount) {
		is = new ItemStack(m, amount);
	}

	/**
	 * Create a new ItemBuilder from scratch.
	 * 
	 * @param m          The material of the item.
	 * @param amount     The amount of the item.
	 * @param durability The durability of the item.
	 */
	public ItemUtil(Material m, int amount, int durability) {
		ItemStack stack = new ItemStack(m, amount);
		ItemMeta meta = stack.getItemMeta();
		Damageable dam = (Damageable) meta;
		dam.setDamage(durability);
		stack.setItemMeta(meta);
		is = stack;
	}

	/**
	 * Clone the ItemBuilder into a new one.
	 * 
	 * @return The cloned instance.
	 */
	public ItemUtil clone() {
		return new ItemUtil(is);
	}

	/**
	 * Change the durability of the item.
	 * 
	 * @param dur The durability to set it to.
	 */
	public ItemUtil setDurability(int dur) {
		ItemMeta meta = is.getItemMeta();
		Damageable dam = (Damageable) meta;
		dam.setDamage(dur);
		is.setItemMeta(meta);
		return this;
	}

	/**
	 * Set the displayname of the item.
	 * 
	 * @param name The name to change it to.
	 */
	public ItemUtil setName(String name) {
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Add an unsafe enchantment.
	 * 
	 * @param ench  The enchantment to add.
	 * @param level The level to put the enchant on.
	 */
	public ItemUtil addUnsafeEnchantment(Enchantment ench, int level) {
		is.addUnsafeEnchantment(ench, level);
		return this;
	}

	/**
	 * Remove a certain enchant from the item.
	 * 
	 * @param ench The enchantment to remove
	 */
	public ItemUtil removeEnchantment(Enchantment ench) {
		is.removeEnchantment(ench);
		return this;
	}

	/**
	 * Set the skull owner for the item. Works on skulls only.
	 * 
	 * @param owner The name of the skull's owner.
	 */
	public ItemUtil setSkullOwner(UUID uuid) {
		try {
			SkullMeta im = (SkullMeta) is.getItemMeta();
			OfflinePlayer pl = Bukkit.getOfflinePlayer(uuid);
			im.setOwningPlayer(pl);
			is.setItemMeta(im);
		} catch (ClassCastException expected) {
		}
		return this;
	}

	/**
	 * Add an enchant to the item.
	 * 
	 * @param ench  The enchant to add
	 * @param level The level
	 */
	public ItemUtil addEnchant(Enchantment ench, int level) {
		ItemMeta im = is.getItemMeta();
		im.addEnchant(ench, level, true);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Add multiple enchants at once.
	 * 
	 * @param enchantments The enchants to add.
	 */
	public ItemUtil addEnchantments(Map<Enchantment, Integer> enchantments) {
		is.addEnchantments(enchantments);
		return this;
	}

	/**
	 * Sets infinity durability on the item by setting the durability to
	 * Short.MAX_VALUE.
	 */
	public ItemUtil setInfinityDurability() {
		ItemMeta meta = is.getItemMeta();
		Damageable dam = (Damageable) meta;
		dam.setDamage(Short.MAX_VALUE);
		is.setItemMeta(meta);
		return this;
	}

	public ItemUtil setUnbreakable() {
		ItemStack stack = is;
		ItemMeta meta = stack.getItemMeta();
		meta.setUnbreakable(true);
		stack.setItemMeta(meta);
		return this;
	}

	/**
	 * Re-sets the lore.
	 * 
	 * @param lore The lore to set it to.
	 */
	public ItemUtil setLore(String... lore) {
		ItemMeta im = is.getItemMeta();
		im.setLore(Arrays.asList(lore));
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Re-sets the lore.
	 * 
	 * @param lore The lore to set it to.
	 */
	public ItemUtil setLore(List<String> lore) {
		ItemMeta im = is.getItemMeta();
		im.setLore(lore);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Remove a lore line.
	 * 
	 * @param lore The lore to remove.
	 */
	public ItemUtil removeLoreLine(String line) {
		ItemMeta im = is.getItemMeta();
		List<String> lore = new ArrayList<>(im.getLore());
		if (!lore.contains(line))
			return this;
		lore.remove(line);
		im.setLore(lore);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Remove a lore line.
	 * 
	 * @param index The index of the lore line to remove.
	 */
	public ItemUtil removeLoreLine(int index) {
		ItemMeta im = is.getItemMeta();
		List<String> lore = new ArrayList<>(im.getLore());
		if (index < 0 || index > lore.size())
			return this;
		lore.remove(index);
		im.setLore(lore);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Add a lore line.
	 * 
	 * @param line The lore line to add.
	 */
	public ItemUtil addLoreLine(String line) {
		ItemMeta im = is.getItemMeta();
		List<String> lore = new ArrayList<>();
		if (im.hasLore())
			lore = new ArrayList<>(im.getLore());
		lore.add(line);
		im.setLore(lore);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Add a lore line.
	 * 
	 * @param line The lore line to add.
	 * @param pos  The index of where to put it.
	 */
	public ItemUtil addLoreLine(String line, int pos) {
		ItemMeta im = is.getItemMeta();
		List<String> lore = new ArrayList<>(im.getLore());
		lore.set(pos, line);
		im.setLore(lore);
		is.setItemMeta(im);
		return this;
	}

	/**
	 * Sets the armor color of a leather armor piece. Works only on leather armor
	 * pieces.
	 * 
	 * @param color The color to set it to.
	 */
	public ItemUtil setLeatherArmorColor(Color color) {
		try {
			LeatherArmorMeta im = (LeatherArmorMeta) is.getItemMeta();
			im.setColor(color);
			is.setItemMeta(im);
		} catch (ClassCastException expected) {
		}
		return this;
	}

	/**
	 * Retrieves the itemstack from the ItemBuilder.
	 * 
	 * @return The itemstack created/modified by the ItemBuilder instance.
	 */
	public ItemStack toItemStack() {
		return is;
	}
}