package eu.minetopiaworld.plugin;

import java.text.SimpleDateFormat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import eu.minetopiaworld.plugin.function.Telefoon;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class ActionBar {
	private static String defaultActionBarMessage = "Datum: <Datum> Tijd: <Tijd>";
	private static String bellenActionBarMessage = "Datum: <Datum> Beltijd: <Beltijd>";

	public void onActionBar() {
		defaultActionBarMessage = ChatColor.translateAlternateColorCodes('&', defaultActionBarMessage);
		bellenActionBarMessage = ChatColor.translateAlternateColorCodes('&', bellenActionBarMessage);
		Player player = Bukkit.getPlayer("");
		if (Telefoon.isBellen() == false) {
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR,
					TextComponent.fromLegacyText(defaultActionBarMessage.replaceAll("<Datum>",
							new SimpleDateFormat("dd-MM-yyyy").toString())
							+ defaultActionBarMessage.replaceAll("<Tijd>", new SimpleDateFormat("HH:mm").toString())));
		}
		if (Telefoon.isBellen() == true) {
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR,
					TextComponent.fromLegacyText(
							bellenActionBarMessage.replaceAll("<Datum>", new SimpleDateFormat("dd-MM-yyyy").toString())
									+ defaultActionBarMessage.replaceAll(null, null)));
			/** replaceAll moet een string method hebben die beltijd opneemt in mm:ss */
		}
	}
}