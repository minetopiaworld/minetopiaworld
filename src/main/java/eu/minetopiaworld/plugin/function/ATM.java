package eu.minetopiaworld.plugin.function;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ATM implements Listener {
	private Inventory atm = Bukkit.createInventory(null, InventoryType.WORKBENCH);
	private Inventory rekening = Bukkit.createInventory(null, 54);
	private Inventory rekeningMenu = Bukkit.createInventory(null, 18);

	// --------------- \\
	// ATM Inventories \\
	// --------------- \\
	private Inventory atmInventory() {
		/** Priv� Rekening */
		ItemStack priveRekening = new ItemStack(Material.GOLD_BLOCK, 1);
		ItemMeta prMeta = priveRekening.getItemMeta();
		prMeta.setDisplayName(ChatColor.GOLD + "Priv� Rekening");
		atm.setItem(7, priveRekening);

		/** Bedrijfs Rekening */
		/**
		 * ItemUtil bedrijfsRekening = new ItemUtil(Material.DIAMOND_BLOCK, 1);
		 * bedrijfsRekening.setName(ChatColor.GOLD + "Bedrijfs Rekening");
		 */
		ItemStack bedrijfsRekening = new ItemStack(Material.DIAMOND_BLOCK, 1);
		ItemMeta brMeta = bedrijfsRekening.getItemMeta();
		brMeta.setDisplayName(ChatColor.GOLD + "Bedrijfs Rekening");
		atm.setItem(8, bedrijfsRekening);
		return atm;
	}

	private Inventory pRekening() {
		return rekening;
	}

	private Inventory bRekeningMenu() {
		return rekeningMenu;
	}

	// -------------\\
	// Click Events \\
	// -------------\\
	@SuppressWarnings("unlikely-arg-type")
	@EventHandler
	public void onATMClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().equals(Material.RED_SANDSTONE_STAIRS)) {
			e.getPlayer().openInventory(atmInventory());
		}
	}

	@EventHandler
	public void inventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory ci = atm;
		if (ci.equals(atm) && e.getSlot() == 7) {
			p.openInventory(pRekening());
			p.sendMessage("Je hebt de rekening van <Speler> geopend.");
		} else if (ci.equals(atm) && e.getSlot() == 8) {
			p.openInventory(bRekeningMenu());
		}
	}
}