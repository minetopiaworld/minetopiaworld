package eu.minetopiaworld.plugin.commands;

/** This class lists currently all items via '/ddgitems' (MTSDB) */
public class DDGItems {
	/** Cars:
	 * - Groen/grijze voertuigen set
	 * - Roze voertuigen set
	 * - Zwarte voertuigen set
	 * - Gele voertuigen set
	 * - Blauwe voertuigen set
	 * - Bruine voertuigen set
	 * - Minetopia bord
	 * - Grijze voertuigen set
	 * - Donkerrode voertuigen set
	 * - Lichtrode voertuigen set
	 * - Lichtroze voertuigen set
	 * - Oranje voertuigen set
	 * - Aqua voertuigen set
	 * - Lichtgele voertuigen set
	 * - Groene voertuigen set
	 * - Paarse voertuigen set
	 * - Lichtgroene voertuigen set
	 * - Zalmroze voertuigen set
	 * - Lichtblauwe voertuigen set (geen aqua!)
	 * - Witte voertuigen set
	 * - Roze voertuigen set
	 * - Lichtgroene voertuigen set
	 * - Wapenstok
	 * - Sweater chestplate
	 * - 1 kleur shirts
	 * - Spijkerbroek
	 * - Sportschoenen
	 * - (Elytra)?
	 * - Legerhelm
	 */
}