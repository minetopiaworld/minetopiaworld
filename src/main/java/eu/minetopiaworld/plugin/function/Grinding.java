package eu.minetopiaworld.plugin.function;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.SpawnReason;
import net.citizensnpcs.api.npc.NPC;

public class Grinding implements GrindShop, Listener {
	@Override
	public void grindShop(NPC npc, Player p) {
		npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.VILLAGER, "GrindSHOP");
		npc.spawn(p.getLocation(), SpawnReason.CREATE);
	}
}