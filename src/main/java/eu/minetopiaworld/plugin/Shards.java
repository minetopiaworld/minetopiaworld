package eu.minetopiaworld.plugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import eu.minetopiaworld.plugin.util.Messages;

public class Shards {
	private static double worldShard = 0.7;
	private static double goldWorldShard = 0.0001;
	private static String worldShardMessage = Messages.WORLDSHARD_GET.get();
	private static String goldWorldShardMessage = Messages.GOLDWORLDSHARD_GET.get();

	public static void startWorldShards(int minutes) {
		worldShardMessage = ChatColor.translateAlternateColorCodes('&', worldShardMessage);
		goldWorldShardMessage = ChatColor.translateAlternateColorCodes('&', goldWorldShardMessage);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(JavaPlugin.getPlugin(Main.class), new Runnable() {
			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					p.sendMessage(worldShardMessage.replaceAll("<WorldShard>", Double.toString(worldShard)));
				}
			}
		}, 0, minutes * 60 * 20);
	}

	public static void startGoldWorldShards(int minutes) {
		worldShardMessage = ChatColor.translateAlternateColorCodes('&', worldShardMessage);
		goldWorldShardMessage = ChatColor.translateAlternateColorCodes('&', goldWorldShardMessage);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(JavaPlugin.getPlugin(Main.class), new Runnable() {
			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					p.sendMessage(
							goldWorldShardMessage.replaceAll("<GoldWorldShard>", Double.toString(goldWorldShard)));
				}
			}
		}, 0, minutes * 60 * 20);
	}
}