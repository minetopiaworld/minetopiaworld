package eu.minetopiaworld.plugin.function;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import eu.minetopiaworld.plugin.Main;
import eu.minetopiaworld.plugin.util.Messages;
import eu.minetopiaworld.plugin.util.Permissions;

public class Deathban implements CommandExecutor, Listener {
	public final int deathbanTime = 60;

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String arg, String[] args) {
		if (cmd.getName().equalsIgnoreCase("deathban")) {
			if (!(s instanceof Player) || Permissions.DEATHBAN_COMMAND.check(s)) {
				boolean sendHelp = false;
				if (args.length <= 0) {
					sendHelp = true;
				}
				if (args[0].equalsIgnoreCase("unban")) {
					if (Permissions.DEATHBAN_UNBAN.check(s)) {
						if (args.length == 1) {
							@SuppressWarnings("deprecation")
							OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
							UUID opUUID = null;
							if (op.hasPlayedBefore()) {
								opUUID = op.getPlayer().getUniqueId();
							} else {
								s.sendMessage(Messages.PLAYER_UNKNOWN.get());
								return false;
							}
							if (Main.activeDeathBan.contains(opUUID)) {
								Main.activeDeathBan.remove(opUUID);
								String unbanMessage = "&2<Player> &aheeft nu geen deathban meer!";
								unbanMessage.replaceAll("<Player>", op.getPlayer().getName());
								unbanMessage = ChatColor.translateAlternateColorCodes('&', unbanMessage);
								s.sendMessage(unbanMessage);
							} else {
								String noDeathBan = "&c<Player> &6heeft geen deathban!";
								noDeathBan.replaceAll("<Player>", op.getPlayer().getName());
								noDeathBan = ChatColor.translateAlternateColorCodes('&', noDeathBan);
								s.sendMessage(noDeathBan);
							}
						}
					}
				} else if (args[0].equalsIgnoreCase("toggle")) {
					// TODO: Toggle command (Als filemanager is een ding)
				} else {
					sendHelp = true;
				}
				if (sendHelp) {
					int subCommands = 0;
					String helpHeader = "&cGeen subcommand opgegeven.";
					String helpHeader2 = "&6/deathban <subcommand <arg>...";
					String scUnban = "&a/deathban &2unban &a<player> &f- &aGeen beschrijving.";
					String scToggle = "&a/deathban &2toggle &f- &aGeen beschrijving.";
					s.sendMessage(ChatColor.translateAlternateColorCodes('&', helpHeader));
					s.sendMessage(ChatColor.translateAlternateColorCodes('&', helpHeader2));
					if (Permissions.DEATHBAN_UNBAN.check(s)) {
						s.sendMessage(ChatColor.translateAlternateColorCodes('&', scUnban));
						subCommands = subCommands + 1;
					}
					if (Permissions.DEATHBAN_TOGGLE.check(s)) {
						s.sendMessage(ChatColor.translateAlternateColorCodes('&', scToggle));
						subCommands = subCommands + 1;
					}
					if (subCommands <= 0) {
						s.sendMessage(Messages.NO_SUBCOMMANDS_ACCESS.get());
					}
					return false;
				}
			} else {
				Permissions.DEATHBAN_COMMAND.send(s);
				return false;
			}
		}
		return false;
	}

	@EventHandler
	public void deathban(PlayerDeathEvent e) {
		Player p = (Player) e.getEntity();
		if (Permissions.DEATHBAN_BYPASS.check(p)) {
			UUID uuid = p.getUniqueId();
			Main.activeDeathBan.add(uuid);
			p.kickPlayer(Messages.DEATHBAN_KICK_1ST.toString());
			Bukkit.getScheduler().scheduleSyncDelayedTask(JavaPlugin.getPlugin(Main.class), new Runnable() {
				public void run() {
					if (Main.activeDeathBan.contains(uuid)) {
						Main.activeDeathBan.remove(uuid);
					}
				}
			}, deathbanTime * 60 * 20L);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void deathbanJoin(PlayerJoinEvent e) {
		if (Main.activeDeathBan.contains(e.getPlayer().getUniqueId())) {
			e.setJoinMessage("");
			e.getPlayer().kickPlayer(Messages.DEATHBAN_KICK.toString());
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void deathbanLeave(PlayerQuitEvent e) {
		if (Main.activeDeathBan.contains(e.getPlayer().getUniqueId())) {
			e.setQuitMessage("");
			e.getPlayer().kickPlayer(Messages.DEATHBAN_KICK.toString());
		}
	}
}