package eu.minetopiaworld.plugin.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public enum Permissions {
	/** Deathban */
	DEATHBAN_BYPASS("mwr.deathban.bypass"), DEATHBAN_TOGGLE("mwr.deathban.toggle"),
	DEATHBAN_UNBAN("mwr.deathban.unban"), DEATHBAN_COMMAND("mwr.deathban.command"),

	/** Rekening */
	REKENING_COMMAND("mwr.rekening.command");

	private String perm;

	Permissions(String perm) {
		this.perm = perm;
	}

	public boolean check(CommandSender player) {
		return player.hasPermission(perm);
	}

	public void send(CommandSender sender) {
		sender.sendMessage(Messages.NO_PERMISSION_COMMAND.get() + ChatColor.translateAlternateColorCodes('&', perm));
	}

	public String get() {
		return perm;
	}
}