# TODO:
# Aflevering 1:
*Alle kleuren zijn in &6 &c (gold, red)*
- Scoreboard: (``Setup gemaakt, nu nog afmaken.``)
  - Locatie (first line)
  - Saldo
  - Level
  - Fitheid (max 225)
    - Start 55
  - WorldShards
- Telefoon(menu)
  - Naam item = - ``<telefoon nummer>`` -
  - Item: blaze powder
  - Worldmaps
    - Zoek op letter
    - Zoek op Categorie
      - Staatsbedrijven
        - Voertuigen Inkoop
          - Start Navigatie
          - (Volgensmij ook terugknop)
            - Navigatie particle trail
            - Scoreboard veranderd
              - Afstand (ipv Saldo)
              - Tijd tot aankomst (ipv Level)
- WorldShard -> 0.7 per x (``Done. Maar storage system nog.``);
- GoldWorldShard -> 0.0001 per x (``Done. Maar storage system nog.``);
- Plotinfo (``Done. Alleen nog functions``);
  - -------------------------------------------------
  - Plotinfo voor: ``<Plotnaam>``
  - Geregistreerd in: ``<Stad>``
  - Eigenaar: ``<Eigenaar>``
  - Leden: ``<Leden>``
  - Doorpoint: ``<Stad>`` - ``<x,y,z>``
  - Standaardplotprijs: ``<Prijs>``
  - Te koop prijs: ``<Prijs>``
  - Flags:``<Flags>``
  - -------------------------------------------------
- Chatformat:
  - `[<Level>] [<Rank>] <DisplayName>`;
  
# Aflevering 2:
- Geld:
  - 1 (Iron Nugget);
  - 10 (Gold Nugget);
  - 50 (Iron Ingot);
  - 100 (Golden Ingot);
  - 200 (Ghast Tear);
  - 500 (Quartz);
  - 1000 (Redstone);
  - 2500 (Emerald);
  - 5000 (Diamond);
  
# Aflevering 3 (de lange uitleg aflevering):
- Plotjes kopen met ``/plot buy``;
- Plotjes verkopen met ``/plot sell <prijs> ``;
  - Wel melding dat deze hoger moet zijn als koopprijs!;
  - Message als je verkoopt: ``Dit plot staat nu te koop! Stop de verkoop met '/plot stopsale'``;
  - Message '/plot stopsale': ``Plot verkoop gestopt!``;
- Plotjes verkopen (snel) met ``/plot quicksell``:
  - ``Weet je zeker dat je je plot voor 80% van de vraagprijs aan de gemeente wil verkopen? Bevestig dit door binnen 10 seconden nog een keer '/plot quicksell' te typen``;
  - Message die volgt: ``Je plot is nu verkocht aan de gemeente voor <Prijs>. Je woont nu nergens meer``; (tenzij je natuurlijk nog meerdere plots hebt)
- Geen pinpassen in-game;
- Telefoon:
  - Bericht: ``je opent de telefoon met telefoonnummer: telefoon nummer``;
  - Beltegoed bericht: ``Het saldo is verhoogd met <saldo> en bedraagt nu <beltegoed>``;
  - Verschillende kleuren mobieltjes;
  - Standaard in 9e slot - niet verplaatsbaar;
  - Telefoon skins (item = iron_ingot + nbt tags);
  - Bug report (item = flint);
  - Afsluiten (item = iron_ingot + nbt tags);
  - Bellen (item = iron_ingot + nbt tags + lore = Versie);
    - Persoon in contactlijst;
    - moet telefoon wel aanstaan;
    - Actionbar veranderd naar: datum + beltijd;
    - Bellen = 50 cent per minuut;
  - Beltegoed (item = iron_ingot + nbt tags + naam = Beltegoed <beltegoed>);
  - Alle berichten (item = iron_ingot + nbt tags);
  - WorldAlerts (item = iron_ingot + nbt tags);
  - Instellingen (item = chest);
    - Items in menu zijn inksac (dye);
    - Gebeld worden door onbekenden:
      - Als dit is ingeschakeld kan je worden gebeld door nummers die niet in je contacten staan.
    - Actieve Route Navigatie:
      - Als dit is ingeschakeld blijft de navigatie telkens de snelste route berekenen.
  - WorldMaps (item = iron_ingot);
    - Zoek op letter (item = iron_ingot); (timestamp: 6:59 voor volledig menu)
      - paar letters invoeren vervolgens op de correcte skull en dan:
        - Navigatie start (item = iron_ingot); (scoreboard verandert zoals aangegeven bij TODO in aflevering 1)
        - Particle trail: fire; (op het einde wordt dat groen)
    - Categorieen (item = iron_ingot);
  - Veiling (item = iron_ingot);
  - Huizenmarkt (item = iron_ingot);
    - Zodra speler voldoende geld heeft komen alle beschikbare huizen in het menu;
  - Ongelezen berichten (item = iron_ingot);
  - Contact personen (item = iron_ingot);
  - Telefoonskins (item = iron_ingot);
  - Boosters (item = iron_ingot);
  - Stemmen (item = iron_ingot);
- ATM (drastisch veranderd, zie timestamp: 7:40):
  - 2 slots:
    - Priv� Rekening (Gold block);
    - Bedrijfs Rekening (Diamond block);
  - Stort message:
    - ``Je hebt <Geld> euro gestort naar de rekening van <Speler>``
- Houdbaarheidsdatum op items:
- Uurloon:
  - 6x per dag
  - Level 99 (david): 1000;
  - Message: ``je hebt succesvol je uurloon ontvangen van <Geld>!``;
- Koelkast:
  - Zorgt ervoor dat datum van voedsel wordt verlengd met het aantal dagen dat het erin ligt
- Deathban:
  - Duur: 1 uur;

# Aflevering 4 (Grinding):
- Scoreboard:
 - Locatie;
 - Saldo;
 - Level;
 - GrindCoins;
 - WorldShards;
- Join Items:
 - Pickaxe (Wooden);
 - Fishing Rod;
 - Flesje water;
 - 32 appels;
 - Telefoon;
- GrindShop:
 - Drinken (Water);
 - Eten (Sinaasappels);
 - Geld ruilen;
 
# Aflevering 5 (Gemeente):
- Commands:
  - /plot info
  - /plot quicksell (80% van plotwaarde)
  - /plot sell
- Veiling
  - Misschien een veiling met /ah ? (of /plot ah)
- Vergunningen
- Creative Plots wordt gebruikt voor bestemmingsplannen die nog niet zijn gebouwd in de stad door de staff zelf
  - Bouw CEO keurt dan het bouwwerk goed of af.

# Aflevering 6 (Huis kopen):
- Zie ook: 1, 3, 5
- /plot stopsale *op het plot*
- Plot verkocht:
  - `&6Je plot is verkocht aan &c<Player> &6voor &c<Prijs>`;
- Kleurvolgorde op tab voor de wijken
- /plot addmember
- /plot removemember

# Aflevering 7 (Wijken in MTW) & Aflevering 10 (Duurste Wijken):
# Aflevering 8 (Level in MTW):
# Aflevering 9 (Politie):
# Aflevering 11 (Auto's Pimpen):
# Aflevering 12 (Eigen kleding): 
# Aflevering 13 (Andere economie):