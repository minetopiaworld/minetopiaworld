package com.mojang.authlib.yggdrasil.request;

import com.mojang.authlib.Agent;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

public class AuthenticationRequest {
	@SuppressWarnings("unused")
	private Agent agent;
	@SuppressWarnings("unused")
	private String username;
	@SuppressWarnings("unused")
	private String password;
	@SuppressWarnings("unused")
	private String clientToken;
	@SuppressWarnings("unused")
	private boolean requestUser;

	public AuthenticationRequest(final YggdrasilUserAuthentication authenticationService, final String username,
			final String password) {
		super();
		this.requestUser = true;
		this.agent = authenticationService.getAgent();
		this.username = username;
		this.clientToken = authenticationService.getAuthenticationService().getClientToken();
		this.password = password;
	}
}