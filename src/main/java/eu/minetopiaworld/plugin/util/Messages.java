package eu.minetopiaworld.plugin.util;

import org.bukkit.ChatColor;

public enum Messages {
	/** No Permission */
	NO_PERMISSION_COMMAND("&cJe mist de permissie "),

	/** Deathban */
	DEATHBAN_KICK_1ST("&cJe bent doodgegaan! Hierdoor heb je een deathban gekregen van 1 uur."),
	DEATHBAN_KICK("&cJe kan de server niet joinen omdat je een deathban hebt."),

	/** Shards */
	WORLDSHARD_GET("&6Je hebt &c<WorldShard> WorldShards &6gekregen omdat je 10 minuten online was"),
	GOLDWORLDSHARD_GET("&6Je hebt &c<GoldWorldShard> GoldWorldShards &6gekregen omdat je 10 minuten online was"),

	/** Player */
	PLAYER_UNKNOWN("&cDeze speler is niet bekend."),
	NO_SUBCOMMANDS_ACCESS("&cJe hebt geen toegang tot een subcommand."),
	NO_SUBCOMMANDS_INPUT("&6/<command> <subcommand> <args...>"),

	/** Plugin **/
	MISSING_DEPENDENCIES("Je mist een vereiste plugin daardoor werkt de plugin niet."),
	NO_KITS_FOUND("&cEr zijn geen join items gevonden voor deze server, ik stel er eentje voor je in!"), 
	INSTALL_GRINDWORLD("Welkom bij MinetopiaWorld! Wil je Grinding activeren? typ dan /grind");

	private String messages;

	Messages(String msg) {
		this.messages = msg;
	}

	public String get() {
		return ChatColor.translateAlternateColorCodes('&', messages);
	}
}