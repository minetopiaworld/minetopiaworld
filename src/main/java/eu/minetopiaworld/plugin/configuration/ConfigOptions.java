package eu.minetopiaworld.plugin.configuration;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import eu.minetopiaworld.plugin.Main;

public class ConfigOptions {
	private static Main plugin;
	private static File messageConfigFile;
	private static FileConfiguration messageConfig;

	// -------------- \\
	// Default Config \\
	// -------------- \\
	public static void setupDefault() {
		// Creating Default 'config.yml'
		String name = plugin.getConfig().getString("configversion");

		// Writing Default 'config.yml'
		plugin.getConfig().set("configversion", name);

		// Save 'config.yml'
		plugin.saveConfig();
	}

	// --------------- \\
	// Messages Config \\
	// --------------- \\
	public static FileConfiguration getMessageConfig() {
		return ConfigOptions.messageConfig;
	}

	/**
	 * We can also make 'createConfig' and define both configs in this method. See:
	 * https://www.spigotmc.org/threads/saveresource-only-callable-from-main-class.113202/
	 * or how MrWouter did it for MinetopiaFarms:
	 * https://github.com/MrWouterNL/MinetopiaFarms/blob/master/src/main/java/nl/mrwouter/minetopiafarms/Main.java
	 */
	public static void createMessageConfig() {
		messageConfigFile = new File(plugin.getDataFolder(), "message.yml");
		if (!messageConfigFile.exists()) {
			messageConfigFile.getParentFile().mkdirs();
			plugin.saveResource("message.yml", false);
		}
		messageConfig = new YamlConfiguration();
		try {
			messageConfig.load(messageConfigFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
}