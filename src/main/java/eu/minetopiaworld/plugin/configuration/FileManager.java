package eu.minetopiaworld.plugin.configuration;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class FileManager {
    static FileManager instance = new FileManager();
    Plugin p;
    FileConfiguration data;
    File dfile;

    public static FileManager getInstance() {
        return instance;
    }

    public void setup(Plugin p) {
        if (!p.getDataFolder().exists()) {
            p.getDataFolder().mkdir();
        }
        this.dfile = new File(p.getDataFolder(), "data.yml");
        if (!this.dfile.exists()) {
            try {
                this.dfile.createNewFile();
            } catch (IOException e) {
            	Bukkit.getLogger().severe(ChatColor.RED + "Data.yml genereren mislukt!");
            }
        }
        this.data = YamlConfiguration.loadConfiguration(this.dfile);
    }

    public FileConfiguration getData() {
        return this.data;
    }

    public void saveData() {
        try {
            this.data.save(this.dfile);
        } catch (IOException e) {
        	Bukkit.getLogger().severe(ChatColor.RED + "Data.yml saven mislukt!");
        }
    }

    public void reloadData() {
        this.data = YamlConfiguration.loadConfiguration(this.dfile);
    } 
}