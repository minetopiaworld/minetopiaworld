package eu.minetopiaworld.plugin;

import static eu.minetopiaworld.plugin.util.Messages.NO_KITS_FOUND;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.earth2me.essentials.Kit;
import com.onarandombox.MultiverseCore.MultiverseCore;

import eu.minetopiaworld.plugin.commands.WijkCommand;
import eu.minetopiaworld.plugin.commands.plots.PlotCommands;
import eu.minetopiaworld.plugin.configuration.ConfigOptions;
import eu.minetopiaworld.plugin.function.ATM;
import eu.minetopiaworld.plugin.function.Deathban;
import eu.minetopiaworld.plugin.function.Grinding;
import eu.minetopiaworld.plugin.util.ItemUtil;
import eu.minetopiaworld.plugin.util.Messages;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin implements Listener {
	public static Main instance;
	private static final Logger log = Logger.getLogger("Minecraft");
	private static Economy econ = null;
	public static List<UUID> activeDeathBan = new ArrayList<>();

	public Main getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {
		ConfigOptions.createMessageConfig();
		ConfigOptions.setupDefault();
		Shards.startWorldShards(10);
		Shards.startGoldWorldShards(10);
		registerCommands();
		registerEvents();
		setupEconomy();
		// onFirstJoinDependCheck(null);
		getMVCore();
		if (!setupEconomy()) {
			log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
	}

	private void registerCommands() {
		getCommand("deathban").setExecutor(new Deathban());
		getCommand("wijk").setExecutor(new WijkCommand());
		getCommand("plot").setExecutor(new PlotCommands()); // TODO: add command to 'plugin.yml'
		// TODO-WIP: getCommand("rekening").setExecutor(new RekeningCommands());
	}

	private void registerEvents() {
		Bukkit.getServer().getPluginManager().registerEvents(new Deathban(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ATM(), this);
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getServer().getPluginManager().registerEvents(new Grinding(), this);
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	@SuppressWarnings({ "null", "unlikely-arg-type" })
	@EventHandler
	private void onJoin(PlayerJoinEvent e, Kit joinItems) {
		// TODO: If no join items exist, create one via Essentials Kits and maybe later
		// also other kit plugins supporten?
		if (getServer().getPluginManager().getPlugin("Essentials") == null
				|| getServer().getPluginManager().getPlugin("EssentialsX") == null) {
			Messages.MISSING_DEPENDENCIES.get();
		} else if (joinItems == null) {
			e.getPlayer().sendMessage(NO_KITS_FOUND.get());
			try {
				if (joinItems.getName().equals("joinitems")) {
					joinItems.getItems().add(new ItemUtil(Material.APPLE, 32).toString());
					joinItems.getItems().add(new ItemUtil(Material.GLASS_BOTTLE).toString());
					joinItems.getItems().add(new ItemUtil(Material.BLAZE_POWDER).toString());
					// TELEFOON <- Moet in laatste vakje staan
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else {
			try {
				joinItems.getItems();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (e.getPlayer().getWorld().equals("Grinding")) {
			BoardManager.grindBoard(null);
		} else {
			BoardManager.defaultBoard(null);
		}
	}

	public MultiverseCore getMVCore() {
		Plugin plugin = getServer().getPluginManager().getPlugin("MultiverseCore");
		if (plugin instanceof MultiverseCore) {
			return (MultiverseCore) plugin;
		}
		throw new RuntimeException("MultiVerse not found!");
	}

	@Override
	public void onDisable() {
		getServer().getPluginManager().disablePlugin(this);
	}
}