package com.mojang.authlib.yggdrasil.request;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

public class RefreshRequest {
	@SuppressWarnings("unused")
	private String clientToken;
	@SuppressWarnings("unused")
	private String accessToken;
	@SuppressWarnings("unused")
	private GameProfile selectedProfile;
	@SuppressWarnings("unused")
	private boolean requestUser;

	public RefreshRequest(final YggdrasilUserAuthentication authenticationService) {
		this(authenticationService, null);
	}

	public RefreshRequest(final YggdrasilUserAuthentication authenticationService, final GameProfile profile) {
		super();
		this.requestUser = true;
		this.clientToken = authenticationService.getAuthenticationService().getClientToken();
		this.accessToken = authenticationService.getAuthenticatedToken();
		this.selectedProfile = profile;
	}
}