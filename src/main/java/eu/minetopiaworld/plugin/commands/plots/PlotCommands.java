package eu.minetopiaworld.plugin.commands.plots;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.block.data.type.Door;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;

import eu.minetopiaworld.plugin.Main;
import eu.minetopiaworld.plugin.util.Messages;

public class PlotCommands implements CommandExecutor {
	@SuppressWarnings("unused")
	private Main plugin;
	private Door door;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("plot")) {
			sendPlotHelpMessage(p);
		}
		if (args[0].equalsIgnoreCase("buy")) {
			if (p.getLocation().equals(null)) { //equals lijst met plots, ophalen van uit .yml?
			}
		}
		return false;
	}

	public void sendPlotHelpMessage(Player p) {
		p.sendMessage(ChatColor.RED + "Geen subcommand opgegeven.");
		p.sendMessage(Messages.NO_SUBCOMMANDS_INPUT.toString().replaceAll("<command>", "plot"));
		// TODO: Command description from 'plugin.yml'?
		p.sendMessage(
				ChatColor.GREEN + "/" + ChatColor.DARK_GREEN + "plotinfo" + ChatColor.WHITE + " - " + ChatColor.GREEN);
		p.sendMessage(
				ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " buy" + ChatColor.WHITE + " - " + ChatColor.GREEN);
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " quicksell" + ChatColor.WHITE + " - "
				+ ChatColor.GREEN);
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " addmember" + ChatColor.GREEN + " <player>"
				+ ChatColor.WHITE + " - " + ChatColor.GREEN);
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " removemember" + ChatColor.GREEN + " <player>"
				+ ChatColor.WHITE + " - " + ChatColor.GREEN);
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " sell" + ChatColor.GREEN + " <player>"
				+ ChatColor.WHITE + " - " + ChatColor.GREEN);
		// p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " setowner"
		// + ChatColor.GREEN + " <player>"
		// + ChatColor.WHITE + " - " + ChatColor.GREEN);
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " list" + ChatColor.GREEN + " <player>"
				+ ChatColor.WHITE + " - " + ChatColor.GREEN);
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " setdoorpoint" + ChatColor.WHITE + " - "
				+ ChatColor.GREEN); //maybe ipv command checken in config waar de deur staat?
		p.sendMessage(ChatColor.GREEN + "/plot" + ChatColor.DARK_GREEN + " stopsale" + ChatColor.WHITE + " - "
				+ ChatColor.GREEN);
	}

	@SuppressWarnings("unused")
	public Door getDoor() {
		ProtectedCuboidRegion wgRegion;
		World world;
		CuboidRegion weRegion = new CuboidRegion(null, null, null);
		for (BlockVector3 block : weRegion) {
		}
		return door;
	}
}