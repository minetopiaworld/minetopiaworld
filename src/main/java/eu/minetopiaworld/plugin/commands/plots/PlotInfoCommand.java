package eu.minetopiaworld.plugin.commands.plots;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlotInfoCommand implements CommandExecutor {
	PlotCommands plotCommand;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (!cmd.getName().equalsIgnoreCase("plotinfo")) {
			sendPlotInfo(p);
		}
		return false;
	}

	/** TODO: Work in progess, after 2nd chat color a function needs to be added */
	private void sendPlotInfo(Player p) {
		p.sendMessage(ChatColor.DARK_GRAY + "-------------------------");
		p.sendMessage(ChatColor.GOLD + "Plot info voor: " + ChatColor.RED + plot.getSubRegionName());
		p.sendMessage(ChatColor.GOLD + "Geregistreerd in: " + ChatColor.RED + plot.getRegionName());
		p.sendMessage(ChatColor.GOLD + "Eigenaar: " + ChatColor.RED + plot.getOwner());
		p.sendMessage(ChatColor.GOLD + "Leden: " + ChatColor.RED + plot.getMembers());
		p.sendMessage(
				ChatColor.GOLD + "Doorpoint: " + ChatColor.RED + plot.getRegionLocation() + plotCommand.getDoor());
		p.sendMessage(ChatColor.GOLD + "Standaard plot prijs: " + ChatColor.RED + plot.getPrice());
		p.sendMessage(ChatColor.GOLD + "Te koop prijs: " + ChatColor.RED + plot.getBuyPrice());
		p.sendMessage(ChatColor.GOLD + "Flags: " + ChatColor.RED + plot.getFlags());
		p.sendMessage(ChatColor.DARK_GRAY + "-------------------------");
	}
}